#include "mafenetre.h"
#include "ui_mafenetre.h"
#include "ODALID.h"
#include <QtGui>

//Les clés A et B permetent de recupérer et de modifier les noms
//les clés C et D permetent d'incrementer et de décrementer
//Les clés B et D sont utilisés pour les informations sensibles(écriture, incrementer)

unsigned char key_A[6] = { 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5 };
unsigned char key_B[6] = { 0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xB5 };
unsigned char key_C[6] = { 0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5 };
unsigned char key_D[6] = { 0xD0, 0xD1, 0xD2, 0xD3, 0xD4, 0xD5 };

BOOL trouble = FALSE;

ReaderName MonLecteur;
uint16_t status = 0;


MaFenetre::MaFenetre(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MaFenetre)
{
    ui->setupUi(this);
}

MaFenetre::~MaFenetre()
{
    delete ui;
}


/**
 * @brief MaFenetre::on_Connect_clicked
 * Fonction qui permet de se connecter au lecteur,
 * il détecte également la présence d'une carte
 * et s'il n'y a pas d'érreur, il charge le nom, le prénom et la valeur du compteur de la carte
 */
void MaFenetre::on_Connect_clicked()
{
    uint8_t atq[2];
    uint8_t sak[1];
    uint8_t uid[10];
    uint8_t serial[4];
    uint8_t nom[16], prenom[16];
    uint16_t uid_len = 10;
    uint32_t recup;
    char version[30];
    char stackReader[20];
    QString enString;


    MonLecteur.Type = ReaderCDC;
    MonLecteur.device = 0;

    status = OpenCOM1(&MonLecteur);

    if(status==0)
    {
       ui->Affichage->append("OpenCOM1");
    }

    //permet de rayonner un champ électro magnétique

    status = RF_Power_Control(&MonLecteur, true, 0);

    if(status==0)
    {
       ui->Affichage->append("champ électromagnétique activé");
    }

    RF_Config_Card_Mode(&MonLecteur, TypeA);

    //chargement des clés
    Mf_Classic_LoadKey(&MonLecteur, Auth_KeyA, key_A, 0);
    Mf_Classic_LoadKey(&MonLecteur, Auth_KeyB, key_B, 0);
    Mf_Classic_LoadKey(&MonLecteur, Auth_KeyA, key_C, 1);
    Mf_Classic_LoadKey(&MonLecteur, Auth_KeyB, key_D, 1);


    Version(&MonLecteur, version, serial, stackReader);
    LEDBuzzer(&MonLecteur, LED_GREEN_ON);

    //recuperer le numéro d'identifiant de la carte permet de savoir si la carte est présente
    status = ISO14443_3_A_PollCard(&MonLecteur, atq, sak, uid, &uid_len);

    if(status!=0)
    {
        LEDBuzzer(&MonLecteur, BUZZER_ON);
        ui->Affichage->append("Defaut de carte");
        LEDBuzzer(&MonLecteur, BUZZER_OFF);
        trouble = TRUE;
    }


//    le rad_sector permet de combiner la partie authenticate et read block


    status = Mf_Classic_Authenticate(&MonLecteur, Auth_KeyA, true, 2, key_A, 0);
    qDebug() << "Mf_Classic_Authenticate A" << status;

    if(status!=0)
    {
        ui->Affichage->append("Problème d'authentification");
        trouble = TRUE;
    }



    // S'il y'a pas d'érreur, on récupère le nom, le prénom et la valeur du compteur
    if(!trouble){

        Mf_Classic_Read_Block(&MonLecteur, true, 9, prenom,  Auth_KeyA, 0);
        for(int i=0;i<16 && prenom[i] != '\0';i++)
            enString.append((char)(prenom[i]));

        ui->fenetre_prenom->setText(enString);
        enString.clear();

        Mf_Classic_Read_Block(&MonLecteur, true, 10, nom,  Auth_KeyA, 0);
        for(int i=0;i<16 && nom[i] != '\0';i++)
            enString.append((char)(nom[i]));

        ui->fenetre_nom->setText(enString);
        enString.clear();

        Mf_Classic_Read_Value(&MonLecteur, true, 14, &recup,  Auth_KeyA, 1);

        ui->credit->display((int)recup);
    }

    ui->version->setText(version);
    ui->version->update();
}


/**
 * @brief MaFenetre::on_Ecrire_Block_clicked
 * Fonction qui permet de modifier le nom et le prénom
 */
void MaFenetre::on_Ecrire_Block_clicked()
{

    uint8_t prenom[16];
    uint8_t nom[16];
    QString enString;
    char DataIn[16];


    //Copy le prénom dans DataIn
    strncpy(DataIn,ui->fenetre_prenom->toPlainText().toUtf8().data(),16);
    Mf_Classic_Write_Block(&MonLecteur, true, 9,(uint8_t *) DataIn,  Auth_KeyB, 0);

    strncpy(DataIn,ui->fenetre_nom->toPlainText().toUtf8().data(),16);
    Mf_Classic_Write_Block(&MonLecteur, true, 9,(uint8_t *) DataIn,  Auth_KeyB, 0);


    //Lecture

    Mf_Classic_Read_Block(&MonLecteur, true, 9, prenom,  Auth_KeyA, 0);
    for(int i=0;i<16 && prenom[i] != '\0';i++)
        enString.append((char)(prenom[i]));

    ui->fenetre_prenom->setText(enString);
    enString.clear();

    Mf_Classic_Read_Block(&MonLecteur, true, 10, nom,  Auth_KeyA, 0);
    for(int i=0;i<16 && nom[i] != '\0';i++)
        enString.append((char)(nom[i]));

    ui->fenetre_nom->setText(enString);
    enString.clear();

}



/**
 * @brief MaFenetre::on_Incrementer_clicked
 * Fonction qui permet d'incrementer le compteur
 * Après l'increment, la valeur n'est pas directement modifier au niveau du compteur, elle est d'abord modifier sur le backup compteur
 * puis à l'aide de la fonction restore value, on copie la valuer du backup compteur dans le compteur
 */
void MaFenetre::on_Incrementer_clicked()
{
    uint32_t recup;
    Mf_Classic_Increment_Value(&MonLecteur, true, 14, 1, 13, Auth_KeyB, 1);
    Mf_Classic_Restore_Value(&MonLecteur, true, 13, 14, Auth_KeyB, 1);
    Mf_Classic_Read_Value(&MonLecteur, true, 14, &recup,  Auth_KeyA, 1);
    ui->credit->display((int)recup);
}

/**
 * @brief MaFenetre::on_Decrementer_clicked
 * Fonction qui permet de decrementer le compteur
 * Après la decrementation, la valeur n'est pas directement modifier au niveau du compteur, elle est d'abord modifier sur le backup compteur
 * puis à l'aide de la fonction restore value, on copie la valuer du backup compteur dans le compteur
 */
void MaFenetre::on_Decrementer_clicked()
{
    uint32_t recup;
    Mf_Classic_Decrement_Value(&MonLecteur, true, 14, 1, 13, Auth_KeyA, 1);
    Mf_Classic_Restore_Value(&MonLecteur, true, 13, 14, Auth_KeyA, 1);
    Mf_Classic_Read_Value(&MonLecteur, true, 14, &recup,  Auth_KeyA, 1);
    ui->credit->display((int)recup);
}

/**
 * @brief MaFenetre::on_Quitter_clicked
 * Methode pour la deconnexion
 * il éteint d'abord le champs électromagnétique du lecteur
 */
void MaFenetre::on_Quitter_clicked()
{
    RF_Power_Control (&MonLecteur, FALSE, 0);
    status = LEDBuzzer(&MonLecteur, LED_OFF);
    if(status==0)
    {
       ui->Affichage->append("LED Eteint");
    }
    status = CloseCOM1(&MonLecteur);
    if(status==0)
    {
       ui->Affichage->append("Fermeture du lecteur");
       ui->Affichage->append("Deconnexion Reussie");
    }

    //qApp->quit();

}

//un peit layous sur les clé,
//ce qu'on a fait
//partire authentification
//l'app
