/**
* @author Cheikh MARONE
* @date Oct 2019
* @chapter Comm sans fil avec Mr Thivent
* @brief Lecture d'une carte avec un lecteur de carte afin de recuperer le nom, le prenom et de créditer ou de décrediter le compteur
*/

#include "mafenetre.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MaFenetre w;
    w.show();
    return a.exec();
}
