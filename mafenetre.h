#ifndef MAFENETRE_H
#define MAFENETRE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MaFenetre; }
QT_END_NAMESPACE

#define Auth_KeyA				TRUE
#define Auth_KeyB				FALSE


class MaFenetre : public QMainWindow
{
    Q_OBJECT

public:
    MaFenetre(QWidget *parent = nullptr);
    ~MaFenetre();



private slots:
    void on_Connect_clicked();

    void on_Quitter_clicked();

    void on_Ecrire_Block_clicked();

    void on_Incrementer_clicked();

    void on_Decrementer_clicked();

private:
    Ui::MaFenetre *ui;
};
#endif // MAFENETRE_H
